%define debug_package %{nil}

Name:           spip
Version:        4.0.8
Release:        1%{?dist}
Summary:        A publishing system for the Internet in which great importance is attached to collaborative working, to multilingual environments, and to simplicity of use for web authors.

License:        GPL
URL:            https://www.spip.net
Source0:        https://files.spip.net/spip/archives/%{name}-v%{version}.zip

Requires:       php webserver 
Recommends:     httpd

%if 0%{?suse_version}
BuildRequires:  unzip
%endif

BuildArch:      noarch
AutoReq:        no 
AutoReqProv:    no

%description
A publishing system for the Internet in which great importance is attached to collaborative working, to multilingual environments, and to simplicity of use for web authors.

%prep
%setup -q -c

%install
mkdir -p %{buildroot}%{_datadir}/%{name}
mkdir -p %{buildroot}%{_sysconfdir}/%{name}

mv  config %{buildroot}%{_sysconfdir}/%{name}/base
cp -aL * %{buildroot}%{_datadir}/%{name}/
rm -rf %{buildroot}%{_sysconfdir}/%{name}/base/remove.txt

cat << '_spip-httpd-conf' > httpd-spip.conf
#This is an example apache file for system-located spip website. The database
#and images will be located in /srv/spip/<name>, while config file will be in
#/etc/webapps/spip/<name>
# To setup the database go to http://spip.example.com/ecrire/
<VirtualHost *:80>
    ServerAdmin spip@example.com
    DocumentRoot "/usr/share/spip"
    ServerName spip.example.com
    <Directory /usr/share/spip>
      DirectoryIndex index.php index.htm index.html
      Options Indexes FollowSymLinks MultiViews Includes
      AllowOverride All
      Require all granted
#If you want to have subfolders like
#http://spip.example.com/first_site/
#http://spip.example.com/second_site/
#add something like that:
#    RewriteEngine On
#    RewriteRule ^(first_site|second_site)$ /$1/ [R,L]
#    RewriteRule ^(first_site|second_site)/(.*) /$2 [QSA,L]

#A generic not garanteed (untested) catch-all:
#    RewriteCond %{REQUEST_URI} !^/(config|ecrire|IMG|prive|plugins|plugins-dist|sites|squelettes-dist|squelettes|tmp|lib|local|mutualisation)/(.*)
#    RewriteRule ^[^/]+/(.*) /$1 [QSA,L]
    </Directory>
</VirtualHost>
_spip-httpd-conf


cat << '_spip-options' > mes_options.php
<?php // /!\ Important: There must be no blank space before &lt;?php or after ?&gt;
// This file was inspired from the spip contrib website
// http://www.spip.net/fr_article3811.html

if ( preg_match(',^/([\.a-zA-Z0-9_-]+)/,', $_SERVER['REQUEST_URI'], $r)
                                       AND !is_dir(_DIR_RACINE . $r[1]) ) {
      $site = $_SERVER['HTTP_HOST'] . '__' . $r[1];
} else {
      $site = $_SERVER['HTTP_HOST'];
}

// Sites are of the form <host> or <host>__<folder> (cf. above)
$rep = 'sites/';
$path = _DIR_RACINE . $rep . $site . '/';

// This will create automatically the folder structure. Disable it and create
// them manually if you wish
if(!is_dir($path)) {
      mkdir($path,0750);
      mkdir($path.'tmp',0750);
      mkdir($path.'local',0750);
      mkdir($path.'IMG',0750);
      mkdir( '/etc/spip/' . $site,0750);
      symlink( '/etc/spip/' . $site, $path . 'config');
}

// The default path for the database is in the config dir. We don't want that
// in /etc
define('_DIR_DB', $path . 'bases/');

// ajout du dossier squelette
if (is_dir($path . 'squelettes'))
      $GLOBALS['dossier_squelettes'] = $rep . $site . '/squelettes';

// spip paths
define('_SPIP_PATH',
        $path . ':' .
        _DIR_RACINE .':' .
        _DIR_RACINE .'squelettes-dist/:' .
        _DIR_RACINE .'prive/:' .
        _DIR_RESTREINT);


// TODO logfiles
//define('_FILE_LOG_SUFFIX', '_' . $site . '.log');
//define('_DIR_LOG',  _DIR_RACINE . 'log/');

// prefixes des cookie et des tables :
$cookie_prefix = str_replace('.', '_', $site);
$table_prefix = 'spip';

// config file (like this one) specific to the site. Note that it can override
// anything you did there, so be careful befor letting anyone modify it.
/*
if (is_readable($f = $path . _NOM_PERMANENTS_INACCESSIBLES . _NOM_CONFIG . '.php'))
        include($f);
*/

// start
spip_initialisation(
        ($path . _NOM_PERMANENTS_INACCESSIBLES),
        ($path . _NOM_PERMANENTS_ACCESSIBLES),
        ($path . _NOM_TEMPORAIRES_INACCESSIBLES),
        ($path . _NOM_TEMPORAIRES_ACCESSIBLES)
);
?>
_spip-options

%post
%{__ln_s} -f %{_sysconfdir}/%{name}/base %{_datadir}/%{name}/config

%files
%{_datadir}/%{name}
%attr(750, apache, apache)%{_datadir}/%{name}/IMG
%attr(750, apache, apache)%{_datadir}/%{name}/local
%attr(750, apache, apache)%{_datadir}/%{name}/tmp
%attr(750, apache, apache)%{_sysconfdir}/%{name}
%license COPYING.txt
%doc CHANGELOG.txt
%doc SECURITY.md
%doc httpd-spip.conf
%doc mes_options.php

%changelog
* Sun Oct 2 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.0.8-1
- Update version

* Sun Mar 6 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.0.5-1
- Update version

* Mon Feb 13 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.0.4-1
- Update version

* Tue Jan 25 2022 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.0.2-1
- Update version

* Mon Dec 20 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.0.1-1
- Add missing unzip for OpenSuse derivatives

* Fri Dec 17 2021 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 4.0.1-1
- Initial package for SPIP
